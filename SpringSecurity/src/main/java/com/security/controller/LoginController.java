package com.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class LoginController {

    //自定义资源授权器
    @Autowired
    MyInvocationSecurityMetadataSourceService filterInvocationSecurityMetadataSource;

    //重新读取DB 刷新系统现有的URL权限缓存，实现动态更新系统的URL权限
    @RequestMapping("/rerole")
    public void rerole()  {
        filterInvocationSecurityMetadataSource.loadResourceDefine();
    }

    @RequestMapping("/login")
    public String login() { return "login"; }

    @RequestMapping("/login-error")
    public String error() {return "error"; }

    @RequestMapping("/index")
    public String index() { return "index";}

    @RequestMapping("/admin")
    public String admin() {return "admin"; }

    @RequestMapping("/user")
    public String user() {
        return "user";
    }

    @RequestMapping("/index2")
    public String index2() {
        return "index2";
    }

    

}
